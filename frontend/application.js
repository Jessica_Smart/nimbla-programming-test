const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const axios = require('axios');
const layout = require('express-layout');
const path = require('path');

app.use(layout());

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs'); 

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(express.static(path.join(__dirname, 'public')));

app.get('/', (req, res) => {
	axios.get('http://localhost:9000/invoices')
		.then(function(response) {
			const invoices = response.data;
			if (invoices.length == 0) {		
				//display page without table
				res.render('homepage', {
					data: {},
					errors: {}
				});
			} else {
				//display page with table
				res.render('index', {
					data: invoices,
					errors: {}
				})
			}
		}
	);
})

app.post('/', (req, res) => {
	if (req.body.id.length == 0) {
		//add new invoice 
		axios.post('http://localhost:9000/invoices', req.body);
	} else {
		//edit existing invoice
		axios.put('http://localhost:9000/invoices/' + req.body.id, req.body); 
	}

	res.redirect('/');
})

app.get('/form', (req, res) => {
	//display form
	res.render('form', {
		data: {},
		errors: {}
	})
})

app.post('/form', (req, res) => {
	//display form with information from invoice to edit
	res.render('form', {
		data: req.body,
		errors: {}
	})
})

app.get('/delete/:id', (req,res) => {
	// delete invoice
	axios.delete('http://localhost:9000/invoices/' + req.params.id);
	res.redirect('/');
});

app.listen(9090, () => {
	console.log('App running at http://localhost:9090');
});
