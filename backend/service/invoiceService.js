const fs = require('fs');
const createError = require('http-errors');

exports.addInvoice = function(newInvoice) {
	console.log('hit service.addInvoice');

	try {
		newInvoice.id = getNextAvailableId();
		fs.writeFileSync('../invoices/' + newInvoice.id, JSON.stringify(newInvoice));
	} catch (err) {
		throwServerError();
	}
};

function getNextAvailableId() {
	var nextAvailableId = getMaxId() + 1;
	return nextAvailableId.toString();
};

function getMaxId() {
	var files = getListOfInvoiceIds();

	if (files.length == 0) {
		return 0;
	};

	return Math.max(...files);
};

exports.deleteInvoice = function(invoiceId) {
	console.log('hit service.deleteInvoice');

	if (fileWithIdExists(invoiceId)) {
		try {
			deleteFileWithId(invoiceId);
		} catch (err) {
			throwServerError();
		}
	} else {
		throwNotFoundError();
	};
};

function fileWithIdExists(invoiceId) {
	var files = getListOfInvoiceIds();

	return files.includes(invoiceId.toString());
};

function deleteFileWithId(invoiceId) {
	fs.unlinkSync('../invoices/' + invoiceId);
};

exports.updateInvoice = function(updatedInvoice, invoiceId) {
	console.log('hit service.updateInvoice');

	if (fileWithIdExists(invoiceId)) {
		try {
			updateFileWithId(updatedInvoice, invoiceId);
		} catch (err) {
			throwServerError();
		}
	} else {
		throwNotFoundError();
	};
};	

function updateFileWithId(updatedInvoice, invoiceId) {
	updatedInvoice.id = invoiceId.toString();
	fs.writeFileSync('../invoices/' + invoiceId, JSON.stringify(updatedInvoice));
};

exports.getInvoices = function() {
	console.log('hit service.getInvoices');

	var invoices = [];
	var files = getListOfInvoiceIds();

	files.forEach(function(invoiceId) {
		addInvoiceToList(invoiceId, invoices);
	});

	return invoices;
};

function addInvoiceToList(invoiceId, invoices) {
	invoices.push(JSON.parse(fs.readFileSync('../invoices/' + invoiceId)));
}


function getListOfInvoiceIds() {
	return fs.readdirSync('../invoices/');
}

function throwServerError() {
	var serverError = createError.InternalServerError("Something went wrong :(");
	serverError.stack = null;

	throw serverError;
};

function throwNotFoundError() {
	var notFoundError = createError.NotFound("Invalid invoice id");
	notFoundError.stack = null;

	throw notFoundError;
};