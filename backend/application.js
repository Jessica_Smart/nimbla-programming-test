const express = require("express");
const app = express();
const bodyParser = require('body-parser');
const invoiceService = require("./service/invoiceService.js");

// app.use('/', controller);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.listen(9000, () => {
	console.log('App running at http://localhost:9000');
});

//Post endpoint at /invoices
app.post('/invoices', (req, res) => {
	invoiceService.addInvoice(req.body);
	res.status(201).send("hi");
});

//Delete endpoint at /invoices/{id}
app.delete('/invoices/:id', (req, res) => {
	invoiceService.deleteInvoice(req.params.id);
	res.status(204).send();
});

//Put endpoint at /invoices/{id}
app.put('/invoices/:id', (req, res) => {
	invoiceService.updateInvoice(req.body, req.params.id);
	res.status(204).send();
});

//Get enpoint at /invoice
app.get('/invoices', (req, res) => {
	const invoices = invoiceService.getInvoices();
	res.status(200).send(invoices);
});